#ifndef PATHTRACE_H
#define PATHTRACE_H
/*------------------------------------------------------------------------------
 This free software incorporates by reference the text of the WTFPL, Version 2

 File:          PathTrace.h

 Description:   The "tracer" locator-node is to be used together with the 
                "pathtrace" command, to create an object motion path in the Maya 
                standard viewport.
                
                The command (defined in pathtrace_cmd.py) creates an instance of 
                this node as well as two standard locators. It makes two 
                connections: from the "translation" attribute of the 1st locator 
                             to the   "pointAttr"   attribute of this node and
                             from the "passThroughAttr" attribute of this node 
                             to the 2nd locator.
 
                This node copies the translation data to passThroughAttr.  The
                connection of passThroughAttr to the second locator forces this
                nodes' compute method to execute.

                ( Based on the Autodesk Maya Plug-in, footPrintNode )

                Created by Ken Mohamed on 6/16/13.
------------------------------------------------------------------------------*/
#include <maya/MPxLocatorNode.h>
#include <maya/M3dView.h>
#include <maya/MPointArray.h>
#include <maya/MDagPath.h>
#include <maya/MDataBlock.h>

// These two error checking macros are based on the similar macros
// in api_macros.h.  Here, messages are written using 
// MGlobal::displayInfo, not cerr <<
//
#define McheckErr(stat,msg)         \
if ( MS::kSuccess != stat ) {       \
    MGlobal::displayInfo( msg );    \
    return MS::kFailure;            \
} 
#define McheckErr_noReturn(stat,msg)\
if ( MS::kSuccess != stat ) {       \
    MGlobal::displayInfo( msg );    \
}   

class PathTrace : public MPxLocatorNode 
{
public:
	PathTrace();
   	virtual ~PathTrace();
    void postConstructor();

    static  void*       creator();
    static  MStatus     initialize();

    virtual MStatus     compute( const MPlug& plug, MDataBlock& dataBlock );
    
	virtual void        draw( M3dView & view, const MDagPath & path,
                             M3dView::DisplayStyle style,
                             M3dView::DisplayStatus status );
    
    static	MString		drawDbClassification;
	static	MString		drawRegistrantId;
    
    // This Node-Type id is a 32bit indentifier that Maya uses to register and
	// recognize this node. Its value must be unique - this is achieved by 
    // obtaining an id from Autodesk.
	//
    static	MTypeId	    id;
    static  MString     NODE_TYPE;
private:
    //Node Attributes 
    static MObject      pointAttr;
    static MObject      passThroughAttr;
    static int          pointAccumulatorSize;

    // Node Instance Internal Data
    // pointAccumulator:  collects point data that appears at the pointAttr attribute 
    // each time the compute method executes.  The draw method ( which executes 
    // once the compute method completes) draws lines between the points that have 
    // accumulated in pointAccumulator.
    // 
    MPointArray  pointAccumulator;
};

#endif
// ----------------------------------------------------------------------------