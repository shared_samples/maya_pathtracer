/*------------------------------------------------------------------------------
 This free software incorporates by reference the text of the WTFPL, Version 2
 
 File:          PathTrace.cpp

 Description:   Plugin Registration/Initialization/Deregistration
  
 
 
                Created by Ken Mohamed on 6/16/13.
  
------------------------------------------------------------------------------*/
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <maya/MTypeId.h>

#include "PathTrace.h"

static MStatus status;

// The Node-Type id is used by Maya to register and recognize this node.
// Its value must be unique - this is achieved by obtaining an id from Autodesk.
// I have use of this block of 64 ID's from Autodesk
// 0x0011EDC0 - 0x0011EDFF
MTypeId PathTrace::id( 0x0011EDC0 );

MString PathTrace::NODE_TYPE = "tracer";

// initializePlugin is called by Maya when the plug-in is loaded.  It creates
// an instance of the MFnPlugin function set to act on it
//
MStatus initializePlugin( MObject object ) 
{
	MFnPlugin pluginFn( object, "kenEhm", "1.0", "Any", &status);
    McheckErr(status, "pluginFn construction failed");
    
	status = pluginFn.registerNode( PathTrace::NODE_TYPE,
                                    PathTrace::id,
                                    &PathTrace::creator,
                                    &PathTrace::initialize,
                                    MPxNode::kLocatorNode,
                                    &PathTrace::drawDbClassification);
    McheckErr(status, "pathtrace registerNode failed");
	return status;
}

MStatus uninitializePlugin( MObject object) 
{
	MFnPlugin pluginFn( object );
    
	status = pluginFn.deregisterNode( PathTrace::id );
    McheckErr(status, "pathtrace deregisterNode failed");
	return status;
}
